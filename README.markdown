#### Table of Contents

1. [Overview](#overview)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)

## Overview

Set a node's hostname with a class parameter.

## Usage

```Puppet
class { '::hostname': hostname => 'foobar', }
```

## Limitations

Currently only supported on Ubuntu 14.04 LTS.
