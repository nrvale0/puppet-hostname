# == Class hostname::params
#
# This class is meant to be called from hostname.
# It sets variables according to platform.
#
class hostname::params {
  unless 'Ubuntu' == $::operatingsystem and $::operatingsystemmajrelease == '14.04' {
    fail("Operating system ${::operatingsystem} ${::operatingsystemmajrelease} not supported!")
  }

  $hostname_file = '/etc/hostname'
  $hostname_command = '/bin/hostname'
}
