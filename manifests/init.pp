# Class: hostname
# ===========================
#
# Modifies $hostname_file with desired hostname and executes $hostname_command.
#
# Parameters
# ----------
#
# * `hostname`
#   The desired hostname.
#
class hostname (
  $hostname
) inherits ::hostname::params {

  file { $hostname_file:
    ensure => file,
    content => $hostname
  } ~>

  exec { "set hostname to $$hostname":
    command => "${hostname_command} ${hostname_file}",
    refreshonly => true,
  }
}
