require 'spec_helper_acceptance'

describe 'hostname class' do
  context 'provide hostname' do
    # Using puppet_apply as a helper
    it 'should work idempotently with no errors' do
      pp = <<-EOS
      class { '::hostname': hostname => 'foobar', }
      EOS

      # Run it twice and test for idempotency
      apply_manifest(pp, :catch_failures => true)
      apply_manifest(pp, :catch_changes  => true)
    end

    describe file('/etc/hostname') do
      its(:content) { should match /^foobar$/ }
    end

    describe command('hostname') do
      its(:stdout) { should match /^foobar$/ }
    end

  end
end
