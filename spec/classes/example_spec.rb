require 'spec_helper'

describe 'hostname' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts
        end

        context "hostname class without any parameters" do
          it { is_expected.to compile.with_all_deps }

          it { is_expected.to contain_class('hostname::params') }
          it { is_expected.to contain_class('hostname::install').that_comes_before('hostname::config') }
          it { is_expected.to contain_class('hostname::config') }
          it { is_expected.to contain_class('hostname::service').that_subscribes_to('hostname::config') }

          it { is_expected.to contain_service('hostname') }
          it { is_expected.to contain_package('hostname').with_ensure('present') }
        end
      end
    end
  end

  context 'unsupported operating system' do
    describe 'hostname class without any parameters on Solaris/Nexenta' do
      let(:facts) do
        {
          :osfamily        => 'Solaris',
          :operatingsystem => 'Nexenta',
        }
      end

      it { expect { is_expected.to contain_package('hostname') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
